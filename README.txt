#LANCER RMIREGISTRY
rmiregistry -J-Djava.rmi.server.useCodebaseOnly=false

#LANCER LE SERVEUR : se situer dans le répertoire JAVARMIServer 
java -cp out/production/JAVARMIServer:../share.jar  -Djava.rmi.server.codebase=file:/home/<chemin vers share.jar>  -Djava.security.policy=security veterinaire.server.Server

#LANCER LE CLIENT : se situer dans le répertoire JAVARMIClient
java -cp out/production/JAVARMIClient:../share.jar  -Djava.rmi.server.codebase=file:/home/<chemin vers share.jar>  -Djava.security.policy=security veterinaire.client.Client
La déconnexion d'un client se fait en tuant le processus par CTRL C (SIGINT). Ceci a pour effet d'appeler la méthode logout() du cabinet

