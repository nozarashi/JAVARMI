package veterinaire.server;

import veterinaire.share.Animal;
import veterinaire.share.IAnimal;
import veterinaire.share.MedicalFile;
import veterinaire.share.Species;

import java.rmi.RMISecurityManager;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Iterator;

public class Server {
    public static void main(String args[]) {

        try {
            Animal a1 = new Animal("Cerbere", "John Doe", new Species("Chien", 13), "Dobermann", new MedicalFile("En pleine forme"));
            Animal a2 = new Animal("Hawk", "Meliodas", new Species("Sanglier", 8), "Porc", new MedicalFile("Ok"));
            Animal a3 = new Animal("Wandle", "Meliodas", new Species("Oiseau", 15), "Toucan", new MedicalFile("Ok"));

            Office cabinet = new Office("Cabinet Katakuri");
            cabinet.addPatient(a1);
            cabinet.addPatient(a2);
            cabinet.addPatient(a3);


            Registry registry = LocateRegistry.getRegistry();
            if (System.getSecurityManager() == null) {
                System.setSecurityManager(new RMISecurityManager());
            }
            if (registry == null) {
                System.err.println("RmiRegistry not found");
            } else {
                registry.rebind("Veto", cabinet);
                System.err.println("Server ready");
                Thread.sleep(10000);

                while (true) {
                    for (int i = 1; i <= 1000; i++) {
                        int index = i % 2;
                        MedicalFile medicalFile = index == 0 ? new MedicalFile("En forme") : new MedicalFile("Malade");
                        cabinet.addPatient(new Animal("animal" + i, "maitre" + i, new Species("espece" + i, 7), "race" + i, medicalFile));
                        if (i == 100 || i == 500 || i == 1000) {
                            System.out.println("Seuil " + i);
                            Thread.sleep(3000);
                        }
                    }
                    Thread.sleep(10000);
                    System.out.println("On retire des patients");
                    int k = cabinet.getPatients().size();
                    for (Iterator<IAnimal> iterator = cabinet.getPatients().iterator(); iterator.hasNext();) {
                        iterator.next();
                        iterator.remove();
                        k--;
                        if(k == 100 || k== 500) {
                            cabinet.notifyAllClients(cabinet.name + " possede " + k + " patients (nb en baisse)");
                        }
                        if (k == 100 || k == 500 || k == 1000) {
                            Thread.sleep(3000);
                        }
                        if(k == 0) {
                            cabinet.notifyAllClients(cabinet.name + " possede " + k + " patients (cabinet vide)");
                        }
                    }
                }
            }
        } catch (Exception e) {
            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();
        }
    }
}
