package veterinaire.server;

import veterinaire.share.IAnimal;
import veterinaire.share.IClient;
import veterinaire.share.IOffice;
import veterinaire.share.Species;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;

public class Office extends UnicastRemoteObject implements IOffice {
    String name;
    private List<IAnimal> patients;
    private List<IClient> connectedClients;

    public Office(String name) throws RemoteException {
        this.name = name;
        patients = new ArrayList<>();
        connectedClients = new ArrayList<>();
    }

    @Override
    public String getInfos() throws RemoteException {
        StringBuilder str = new StringBuilder("\nCabinet :" + this.name + "\nListe des patients \n");
        for (IAnimal p : patients) {
            str.append(p.getInfos());
        }
        return str.toString();
    }

    @Override
    public void setMedicalFile(IAnimal animal, String state) throws RemoteException {
        if (animal != null) {
            animal.setMedicalFile(state);
        }
    }

    public void addPatient(IAnimal animal) throws RemoteException {
        boolean added = patients.add(animal);
        int nb = patients.size();
        if (added){
            switch (nb) {
                case 100:
                    notifyAllClients(name + " possede 100 patients (nb en hausse)");
                    break;
                case 500:
                    notifyAllClients(name + " possede 500 patients (nb en hausse)");
                    break;
                case 1000:
                    notifyAllClients(name + " possede 1000 patients (nb en hausse)");
                    break;
            }
        }
    }

    public void removePatient(IAnimal animal) throws RemoteException {
        boolean removed = patients.remove(animal);
        int nb = patients.size();

        if (removed) {
            switch (nb) {
                case 100:
                    notifyAllClients(name + " possede 100 patients (nb en baisse)");
                    break;
                case 500:
                    notifyAllClients(name + " possede 500 patients (nb en baisse)");
                    break;
                case 1000:
                    notifyAllClients(name + " possede 1000 patients (nb en baisse)");
                    break;
            }
        }
    }

    @Override
    public IAnimal findPatientByName(String name) throws RemoteException {
        for (IAnimal p : patients) {
            if (p.getName().equals(name)) {
                return p;
            }
        }
        return null;
    }

    @Override
    public void foo(Species species) throws RemoteException {
        System.out.println("Espece de classe "+ species.getClass().getSimpleName());
    }

    @Override
    public void connect(IClient client) throws RemoteException {
        connectedClients.add(client);
        System.out.println("Nouveau client connecte");
    }


    @Override
    public void logout(IClient client) throws RemoteException {
        connectedClients.remove(client);
        client.alert("Deconnexion...");
        System.out.println("Un client s'est deconnecte");
    }

    @Override
    public List<IAnimal> getPatients() throws RemoteException {
        return patients;
    }

    @Override
    public List<IClient> getConnectedClients() throws RemoteException {
        return connectedClients;
    }

    public void notifyAllClients(String msg) throws RemoteException {
        for (IClient c : connectedClients) {
            c.alert(msg);
        }
    }

}
