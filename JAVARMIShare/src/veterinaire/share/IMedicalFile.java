package veterinaire.share;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IMedicalFile extends Remote {
    public String getInfos() throws RemoteException;
}
