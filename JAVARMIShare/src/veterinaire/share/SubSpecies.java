package veterinaire.share;

import veterinaire.share.Species;

public class SubSpecies extends Species {
    private String description;

    public SubSpecies(String name, int lifeExp, String desc) {
        super(name, lifeExp);
        description = desc;
    }

    @Override
    public String toString() {
        return super.toString() + ", description : "+description;
    }
}
