package veterinaire.share;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface IOffice extends Remote {

    public String getInfos() throws RemoteException;

    public void setMedicalFile(IAnimal animal, String state) throws RemoteException;

    public void addPatient(IAnimal animal) throws RemoteException;

    public void removePatient(IAnimal animal) throws RemoteException;

    public IAnimal findPatientByName(String name) throws RemoteException;

    public void foo(Species s) throws RemoteException;

    public void connect(IClient client) throws RemoteException;

    public void logout(IClient client) throws RemoteException;

    public List<IAnimal> getPatients() throws RemoteException;

    public List<IClient> getConnectedClients() throws RemoteException;
}
